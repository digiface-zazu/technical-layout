# Documentation DIGIFACE

version 0.0.3

## 0. General Introduction

DIGIFACE is the web-platform, that serves the African-Centres of Excellence as a digital community, learning, project and publication tool.
It offers these functions:

1. communication system like WhatsUp or Threema
2. personal profile system like LinkedIn
3. news and articles through a WordPress blog system
4. completely integrated Moodle learning tool
5. project management tool for big and small project and international research
6. publication tool for academic publications (reliable African knowledge base)
7. access to all functions through a browser or the African-Excellence app

## 1. Frontend Description WP

## 2. Backend Description WP

## 3. Security and Login WP, Moodle and the App

### 3.1. WordPress general Login System

### 3.1.1 Cookies

When a user logs in to a WordPress site, a number of cookies are created. These are used to verify the identity of the logged-in users. WordPress uses security keys and salts to give you a cryptic output that’s stored in the database or cookie, adding a layer of security.

Two of these cookies are:

- WordPress_[hash] used only on the admin page or the WordPress dashboard.
- WordPress_logged_in_[hash] used throughout WordPress to determine whether or not you are logged in to WordPress.

WordPress support:

WordPress uses the two cookies to bypass the password entry portion of wp-login.php. If WordPress recognizes that you have valid, non-expired cookies, you go directly to the WordPress Administration interface. If you don’t have the cookies, or they’re expired, or in some other way invalid (like you edited them manually for some reason), WordPress will require you to log in again, in order to obtain new cookies.

[here you can create new hashes](https://api.wordpress.org/secret-key/1.1/salt/)

|define|hash|
|------|----|
|define('AUTH_KEY',|          `'63 random digits');`|
|define('SECURE_AUTH_KEY',|   `'63 random digits');`|
|define('LOGGED_IN_KEY',|     `'63 random digits');`|
|define('NONCE_KEY',|         `'63 random digits');`|
|define('AUTH_SALT',|         `'63 random digits');`|
|define('SECURE_AUTH_SALT',|  `'63 random digits');`|
|define('LOGGED_IN_SALT',|    `'63 random digits');`|
|define('NONCE_SALT',|        `'63 random digits');`|

These are:

1. AUTH_KEY can be used to make changes to the site. It helps to sign the authorizing cookie for the non-SSL.
2. SECURE_AUTH_KEY is used to sign the authorizing cookie for SSL admin and is used to make changes to the website.
3. LOGGED_IN_KEY is used to create a cookie for a logged-in user. It cannot be used to make changes to the site.
4. NONCE_KEY is used to sign the nonce key. This key protects the nonces from being generated, thereby protecting the site from being attacked.

WordPress salts are random strings of data that are added to the security keys and add an extra layer of protection to the site.

For the cookies the authentication details (both the username and password) are hashed using a set of random values specified in the WordPress security keys
WordPress security keys are used to secure the site’s cookies.

```php
wp_salt(string $scheme = 'auth')
```

Returns a salt to add to hashes.

Salts are created using secret keys. Secret keys are located in two places: in the database and in the wp-config.php file. The secret key in the database is randomly generated and will be appended to the secret keys in wp-config.php.

Open Question

> How are these cookie keys generated in detail ?

#### 3.1.1.1. Example Cookies from DIGIFACE

cookie: `wordpress_logged_in_2e8c4acde0db4dcff31f41674e03335d`

value: `real-user-name-stands-here%7C1606151189%7CFHflCfHNOE138C0fISnhyQyP4z6cnsBvEE5NolH3boF%7C994fa77feb0738b6a263effb44f27c9ddb5c98620d6d62cd48aa6ddf2ebd2d60`

cookie: `wfwaf-authcookie-88%7Cadministrator%7Cde64ca32f5473ff5b5a8ba65e02ef77c3445239cd87f79eeb61ff017f21d5c4c`

value: `wordpress_sec_2e8c4acde0db4dcff31f41674e03335d`

cookie: `wordpress_sec_2e8c4acde0db4dcff31f41674e03335d`

value: `real-user-name-stands-here%7C1606151189%7CFHflCfHNOE138C0fISnhyQyP4z6cnsBvEE5NolH3boF%7C7a3321a93fd78892011209bea259532d598ab53f032f38fe9d34606fc5479cfb`

Updating the WordPress security keys and salts will invalidate all existing cookies, causing all users to be instantly logged out.
But you can update it any time without problems or even do it with the scheduler on a regular base (e.g. every 6 months) or with a plugin e.g. Salt Shaker.

#### 3.1.2. Login and Hash storage in the data base

##### 3.1.2.1 Basics

In the database table **"wp_users"** you will find the hashed password of a user:

|ID|user_login|user_pass|user_nicename|user_email|
|--|----------|---------|-------------|----------|
|100|Thomas|$P$BmLX7xxXdIbMxl2VEBeNJ/9oP.5d8hp|thomas||

|user_url|user–registered|user_activation_key|user_status|display_name|
|--------|---------------|-------------------|-----------|------------|
||2020-05-11 09:01:22|yyyyy:$P$xxxxxxxxx|0|Thomas|

user_activation_key

user_activation_key is one of those keys that WordPress uses for authenticity of a Reset Password request.
So, when you are saying this key is empty for some users, the reason is, those users have never requested for a password reset.
When a password reset is requested a temporary key is generated called check_password_reset_key and sent via email for the reset process.
When the user goes to the generated link, this key is directly compared with user_activation_key. If both keys don't match then it is considered to be an Invalid Request. Otherwise it proceeds and the user will be allowed to enter a new password for his account.

##### 3.1.2.2 Generating the Hash

- MD5 Hash: $1$12345678901234567890123456789012
- MD5 with salt: $1$123456$8901234567890123456789012
- normal MD5: 12345678901234567890123456789012
- PHP Hashing phpass: $P$1234567890123456789012345678901

Hashing structure:

``
$id$salt$encrypted
``

"id" = hash-method:

- 1 = MD5
- 2a = Blowfish
- 5 = SHA-256
- 6 = SHA-512
- P = phpass

**WordPress is using "phpass" to create hashes**,

but there is also a fallback, that you can use MD5. If you use MD5 WordPress will re-hash it with the next login, into a phpass-hash.
You can change your password manually in the wp_users data base table and hash it to MD5 with phpmyadmin or insert a MD5 sting right away, after that WordPress will hash it again:

[more on stackoverflow …](https://stackoverflow.com/questions/1045988/what-type-of-hash-does-wordpress-use)

> WP will still take the straight MD5 the first time you used the Password, then it will "salt" it. So, if you have access to the DB, using MyPHPAdmin you can change the PW to "MyPass", select MD5 in the "Function" dropdown and it will save as a straight MD5. Sign into Wordpress, and it will change it to the "salted" version with the $P$B__/ added.

next:
> Basically, if the user has NOT signed in with the PW, it will still be in MD5 format, it will look the same as anyone else with that same PW. Once you sign in, even with that SAME EXACT PW, it will change to YOUR own personal "salt", so you and your friend, sign in at the same time with the same PW, your "salted MD5" will look different. Meaning, there is no way to query the DB to test if a PW has been changed from the default, since WP will change it to a salted version, even if it was the same as the default.

Important to know:

> Is this true?

WordPress uses Base64 to represent the hash since it has 25% less characters. But it changes human readabilty.

WordPress uses a combination of MD5 and PHPass to hash the passwords. In the PHP hashing system, by using CSPRNG (random number), a salty password that is random will be created. (In older versions of WP it was a 8 time MD5 hash.)

##### 3.1.2.3 How the hashing works

> Is this true?

To create hashed WordPress password, the bcrypt method could be useful. Bcrypt is the default method in WordPress. Both wp_hash_password and p_set_password are connectable. Thus, you can do the process yourself.

```php
function wordpress_hash_password( $password ) {
 require_once( '/path/to/wp-includes/class-phpass.php' );
 $wp_hasher = new PasswordHash( 8, TRUE );
 $hashed_password = $wp_hasher->HashPassword( $password );
 return $hashed_password;
}
```

> Is this true?

Thomas understanding:

1. create a MD5 hash in 8 rounds
2. calculate a salt using a random number
3. hash the 8 times MD5 hash with PHPass into the fnal hash
4. the output should be "$P$ *salt 22 characters* $ *hash 31 characters*"

$P$[22 character salt]$[31 character hash]

but in the data base of DIGIFACE we have:

$P$[31 character hash]

> What is wrong here?

### 3.2. WordPress connection to Moodle

> alternative to Edwiser is WP OAuth server plugin: "Connects Moodle LMS and use WordPress users." / But no selling option for Moodle courses integrated

The connection between Moodle sign-in and sign-out is and WP sign-in and singn-out is managed by the WordPress plugin: Edwiser Bridge Single Sign On.

3.2.1 The process

- A common key is set inside the Moodle and inside the WordPress plugin
- signing out of Moodle will trigger a signing out in WordPress and vice versa, same for signing in
- on both sites you can set an URL to redirect to, after signing out or signing in
- WordPress uses the WooCommerce My Account login page
- you have the possibility of using the google or facebook OAuth server for logging in, the OAuth server creates a client ID and a Secret Key that you copy to the Edwiser plugin, from here on it is OAuth.
- you can sell Moodle courses with WP WooCommerce and direct users to the course after the payment is done

3.2.2 Setting up the service

a. WordPress

- install the plugin

b. Moodle

- install the wdmwpmoodle plugin into the "auth" folder of Moodle
- add the "wdm_sso_verify_token" service

c. setting up the connection

- add the same secret key ("password") in the Moodle plugin and in the WP plugin
- if someone was loged in with WP and logs out of Moodle you can set a link to WP after logging out of Moodle and present there a link to go back to Moodle, without the need to log in again
- the plugin also supports OAuth through social media
- you can setup redirects according to user-roles
- add "[wdm_generate_link]" to any page to present a "Go to Moodle button"

![sso server-client on both sites](Images/Security/sso-edweiser.png)

Open Questions

- I assume, that on both sites there is a server and a client and they can use both roles
- I assume, that OAuth 2.0 is used
– Are passwords and user credentials in both data bases or only in one?
- Do we always use WP as entrance gate, to have all user credentials on the WP site?
- is behind the WP Edwiser plugin a WP OAuth 2.0 plugin?

> How is the process really?

### 3.3. WordPress connection to the App

#### 3.3.1. Best Practice

> This section has to be approved by Jema!

1. Do authentification with OAuth 2.0 using OpenID Connect, on the server side use the OAuth-Point of the plugin Edwiser (or a WP OAuth 2.0 plugin) to use the DIGIFACE server as authorisation (OAuth 2.0) and authentification (OpenID Connect) server.
2. Use the access token that you gain in the authorisation process (together with the ID token) to keep the access to the DIGIFACE server for the app going.
3. Use the new GraphQL API schema to realize the API.

> Why GraphQL and not RPC-api or REST api?

[-> check out this video for different APIs and where to use what](https://www.youtube.com/watch?v=IvsANO0qZEg)

WordPress supports natively RPC and REST

-> [WordPress documentation APIs](https://codex.wordpress.org/WordPress_APIs)

WordPress has a free open-source plugin to support GraphQL, but not natively

-> [WordPress GraphQL API information](https://www.wpgraphql.com/)

#### 3.3.2. Main reason for using GraphQL

GraphQL was build to overcome the heavy overload and the need for multible server request in low bandwidth setting, like bad connection to your phone. It was created for facebook who and their special demands of getting fast the conversions and the facebook wall of a user on the display screen. Same problem what our app will encounter. So GraphQL will probably be the best match.

#### 3.3.3. Drawback about GraphQL

- Lot of work to build the API schema first.
- no native support through WP

![which API for what](Images/API/apis-compare.png)

#### 3.3.4. Alternative API is RPC

Has a strong connection to the structure on the delivery server, but is very fast and efficient.
Basically it is sending function names via HTTP to the server to perform.

![more information on APIs](Images/API/api-more-information.png)

#### 3.3.5 Decision in December 2020

Jonathan:

Since we are working with WordPress as the backend, we are using the WordPress API which is a rest API that already comes with some standard endpoints for fetching and sending data. GraphQL works in the way that you send a request to only one endpoint and you can only send POST requests as opposed to being able to GET, PUT, POST etc. While GraphQL may result in smaller payloads and prevent over-fetching in some cases as a result, I don’t know that dropping the existing WordPress API is the answer here. You mentioned in the doc that you could make one GraphQL call instead of 10 REST calls. 

Yes, but can you think of a time you would actually make so many REST calls at once? To fetch the blog posts you will make one request. Tapping on a particular blog article will make another single REST call to that ID. Maybe 2 if you wanted to include comments there because I think there is a separate endpoint for comments. For me, this doesn’t feel like enough justification to drop the existing WordPress API in favour of GraphQL just so you can send the blog post and the comments in one query.  This may be a trivial example but you can see what I am getting at. While GraphQL is gaining popularity and people are moving over to it, REST API is still the ‘industry standard’ and I feel is a good choice for this project. If we were not using WordPress, then it might have made more sense to use GraphQL because then you are starting from scratch with the API.

## 4. Functions of the App

1. Login
2. User-Profile -> view, write, delete
3. Blog -> view, write, delete
4. internal Publicatins -> view, write, delete
5. Moodle
6. Project Management

-> to be completed and extended in more detail

## 5. Technical Layout of WP

### 5.1. Layout

### 5.2. Structure

#### 5.2.1. Entities of the USER profile

USER

(without technical fields and without fields for visibility to world, members or none)

technical

- DIGIFACE user ID
- Password
- 2 factor authentication (boolean)
- please certify me (boolean)
- language (for the BE)
- profile picture background
- profile picture

general

- professional Title
- Gender
- username
- First Name
- Last Name
- Email
- Website
- user status (e.g. student, prof.)
- user role (on the platform)

addresses

city (old: will be calculated form address)
location (old: will be calculated from address)

- use for centre of living priv./prof. address
- private phone
- private mobile phone
- private email
- private web
- private street plus number
- private street additional
- private zip
- private city
- private country
- private details text

- prof. phone
- prof. mobile phone
- prof. email
- prof. web
- prof. street plus number
- prof. street additional
- prof. zip
- prof. city
- prof. country
- prof. details text

descripion and additional

- description
- slogan
- additional information (text)
- video about myself
- African-Centre of Excellence I belong to
- DAAD ID
- show birth date (boolean)
- birth date
- personal info text

list information

- qualifications (list: title, year, approved, approved person id, approved timestamp)
- current professional activities (list: position, institution)
- past professional experiences (list: type of work, position, start date, end date or current, description) 
- language abilities (list: language, speaking INT, writing INT)
- keywords (list: keyword)
- image slider (list: image, caption)
- internal publications (list: description, copy right, keywords (list: keyword), file)
- external publications (list: year, description, link)

activities

- chosen Moodle courses (list: course name)
- recommended Moodle courses (list: course name)
- blog author (list: blog ID)
- projects involved (list: project ID)
- projects leader (list: projects ID)

social media (list to be confirmed and should be more academic)

- Facebook
- Instagram
- LinkedIn
- MySpace
- Pinterest
- SoundCloud
- Tumblr
- Twitter
- YouTube
- Wikipedia

from Projects

- enable project manager daily digest (boolean)
- notify new projects (boolean)
- update projects (boolean)
- new message (boolean)
- new comment (boolean)
- update comment (boolean)
- new task (boolean)
- update task (boolean)
- complete task (boolean)

(FUTURE) from Communication

- friends (list: friend id)
- groups (list: group id)
- friends online (list: friend id)
- calculated amount of normal messages not read (INT)
- calculated amount of friends messages not read (INT)
- calculated amount of official messages not read (INT)

### 5.2. Database of WP

#### 5.2.1 The WordPress data base tables

##### WordPress general and user

![general WP](Images/Data-Base/data-base-1.png)

##### woocommerce tables

![woocomemerce](Images/Data-Base/data-base-2.png)

##### project management plugin tables

![project management](Images/Data-Base/data-base-3.png)

##### Budypress community plugin tables

![community budypress](Images/Data-Base/data-base-4.png)

##### micellaneous tables

![miscellaneous](Images/Data-Base/data-base-5.png)

##### wordfence plugin tables

![wordfence](Images/Data-Base/data-base-6.png)

## 6. Technical Layout of Moodle

## 7. Technical Layout of the App

### 7.1. Basic concepts

### 7.1.1 Communication with the server and authentication

1. In case of registration and login, the app uses the same functionality as it is already established on the platform and the platform must provide some APIs that trigger that process. There is no direct access to Moodle or to the database of the platform from the app.
2. Queries are done only through WordPress and result then transferred according to the request back to the app. App is just giving requests and platform forwarding everything.
3. Since the app will have the same core-functionality as the platform, APIs should access to the complete business logic of the platform (blog, user profile, Moodle, …)
4. Jema and Avik suggested that the platform is using the already existing business logic that now serves the browser to serve a request of the app.

## 8. Hardware

The hardware infrastructure and design of the DIGIFACE servers and virutal servers. Provided by Eldridge van der Westerhuizen.

![hardware infrastructure](Images/Hardware/DigiFace-Infrastructure-Design-greyed-out.jpg)

Full image with IP-addresses and names are available.

## 9. General Information

## 10. Best Practice and Coding Guidelines

## 10. Legal Information

## 11. The Team behind DIGIFACE
