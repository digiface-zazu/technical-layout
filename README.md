# Technical Description of the DIGIFACE platform

zazu.berlin Thomas Hezel

View the latest version of the specification document on [Bitbucket](https://bitbucket.org/digiface-zazu/technical-layout/src/release)

The specification of the implementation is described in the document: technical-layout-digiface.md

Changes made during implementation are listed here: changes-protocol.md

- The release branch has the latest version.
- The master branch has a pdf-file for easy reading, but doesn't have always the latest changes.
