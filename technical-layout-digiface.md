# Technical Description of the DIGIFACE platform

V 1.0.0 / zazu.berlin Thomas Hezel 20200829
View the latest version of this document on [Bitbucket in the release branch](https://bitbucket.org/digiface-zazu/technical-layout/src/release/)

## 0. Basic

German taxpayers money can not be used to create a system that violates German and European laws.
The African Excellence digital Platform must comply with the EU General Data Protection Regulation (GDPR).

## 1. HOME

entry point to the DIGIFACE platform

### 1.1. Header

1. Left top, language.
    - The installation is setup with English and French but should later be able to handle more languages. The header menu changes to the corresponding language page and internal language setup for all modules
    - language should change in BE and Frontend
    - language should be handeled manually (no DeepL or Google) for all elements and all content, since we deal with scientific work and have to be precice and controlled
2. Top right, login.
    - Link to a central login page for all modules and parts of the project. (Login with Word Press into Moodle. Login with Word Press into WP project management. But we have GDPR rules, so NO login with facebook or Google. Don't send ANY data to America!) Recommended: OAuth. Please hide technical roles, that are needed only for processing, and don't have a logical impact, from the backend admins (if possible)
    - user roles
        1. guest
        2. standard user
        3. member
        4. member admin
        5. admin
        6. sudo admin
        7. Moodle user roles …
        8. WP project management user roles (Manager/Co-Worker)

    - user roles described
        1. guest: May attend open Moodle courses and nothing else. Anyone can become a guest by simply logging in, without manual confirmation. He /she may also participate in individual projects or courses (e.g. as a guest professor, guest learner or guest researcher). There is no permission to write blogs.
        2. standard user: May participate in all Moodle courses, in all functions and in all projects. But there is no permission to write blogs.
        3. member: A Member may participate in every functionality and also write blogs, may create projects and create Moodle courses, can take on all roles in a Moodle course or in research projects.
        4. member-admin: may take part in everything what a Member is allowed to do, plus verify CVs, confirm external certificates of students, make corrections in the status of all subordinate users, make manual corrections in the user profiles, may block or delete users. There is  one Member-Admin per CoE
        5. admin: The Admin confirms Member-Admins and can use all functions. This function is given to the Control Centre which controls and maintains the website, is responsible for general editing, newsletter, PR etc.
        6. sudo admin: technical administration for everything, "superUserDo = sudo", responsible for updates, bugs and backups.

3. Header Logo and opening text
    - The header-text is a general text about the project. We don't need this text for logged in users. If logged in there is a different text content, e.g. important news or a message to the members of the platform. Admin editors should be able to change it.
    - Logo: use embedded SVG to reduce server requests.
4. Menu with drop down area for the sub-menu
    - see the hidden layers in the photoshop layout document for the drop-down menu
    - the desktop big screen menu is a slider to accommodate enough main menu points, for mobile it is a list with sub-lists
    - for the icons, please use embedded SVGs to reduce server requests
5. Breadcrumb navigation underneath the header

#### Page layout

- 1 Home
  - About DIGIFACE
  - FAQ
- 2 Programmes general page
  - About African Excellence
    - single Centre page
      - Partners
      - Contact
      - additional page if required
    - single Centre page
      - Partners
      - Contact
      - additional page if required
  - About any other Programme that takes part
    - single other programme page
      - Partners other programme
      - Contact other programme
      - additional page if required for other programme
    - single other programme page
      - Partners other programme
      - Contact other programme
      - additional page if required for other programme
- 3 Blog list of Blogs
  - Single blog article
- 4 Learn WP page about Learning
  - Moodle starting page
  - Moodle subpages
  - …
- 5 Publications start page with search
  - categories list page (each category)
  - search result page
    - single publication page
- 6 Projects start page overview
  - single project page
    - pages inside a project (issue board like Asana)
    - …
- 7 Members list start page
  - members search result pages
    - single member profile page
    - each member has a page …
- 8 Alumni-Network (alumni external page)
  - alumni subpages (external)
- 9 general search results page
- 10 Login/logout
- 11 Legal notice
- 12 Data protection
- 13 Backend pages
  - my profile backend
  - … more backend pages …

### 1.2 Main Content of Home

1. element: Slogan
    - a slogan for the platform
    - for the state "logged-in" this changes, according to the header description text, into a brief news alert or it is not displayed, on "isempty"
2. element: Latest Blogs
   - displays in desktop view the 4 latest blogs, but it is a slider, slide arrows are displayed top right, responsive on mobile view just one blog preview full width
   - "search and view all" is a link to the general blog page with an anchor to the open search area.
   - **The layout of the blogs preview has changed please see the blogs preview on the layout pdf for "Blogs"! Now ~~social media (may be later)~~, category, date and authors name are added!**
3. element: recommended topics
   - this is a on site menu, that should be editable by the admin, it slides on request and if bigger than 5 entries for desktop view
4. element: Our Centres in Africa
   - this SVG navigation map plus accordion has two possibilities to look up a centre
       1. SVG map: hover over the dots on the map will display the corresponding Centre listed underneath the map. The hover effect changes the Centre displayed will stay, so I have the change to click on the link behind the Centres name.
       2. a click on the dot will also link to the corresponding Centre
       3. the accordion column is a normal accordion with a link displayed to the page of the corresponding Centre
5. element: two column information about the programme
    - the more-link goes to the Centres main page, how to apply is the same link
6. element: 3 column display of RANDOM members
   - content: picture, Name, first two entries on "current professional activity" -> see members/single member
   – "~~search and view all~~" is now a drop-down menu for a simple keyword-search. Caption is "search for members".
   - the circle symbol reloads the random display of members, here on HOME it is any member (different on other pages, there could be restrictions to a group of members .e.g. just from a certain Centre)
7. element: News from the Alumni Network Tumefika
    - This is blogs (news) on our system, made on our system, meaning another element that shows our blogs, but limited/sorted to the blog category „alumni“. Just the same element like on other places but with a different sorting.
    - this is again a display of news, but only news from the category "alumni"
    - it is a slider
    - on top right is a "Links to Tumefika", hover (mobile: click) opens a menu with the top level menu items of the external [Tumefika homepage](https://www.aen-tumefika.org/)
    - for the drop-down menu please enable the layers in the photoshop design document
    - This is a drop down menu, that shows normal links to the Tumefika webpage. So a menu with external links. (They will also establish the same on their website with links to us. We try to integrate both sites through links.)
8. element: recommended Moodle courses
   - displays in two column Moodle courses having the tag "display on home", this tag must be created inside Moodle
   - the "more" link goes to a course page
   - the "search for courses" (sometimes in the design called "search and view all") is a link to the Moodle start page with an anchor to the search area inside Moodle. Results are displayed on the corresponing Moodle page. Caption is: "search for courses"
   - Go to Moodle database and search for courses that have the show-on-WordPress-field ticked with "true". Show these courses in a "slide show" according to the responsive view (mobile just one-by-one. A Moodle plugin must add the "show-on-home-field" to every course. A field that can only be ticked by admins.

### 1.3 Footer Home

1. two columns contact details and logos
2. float right, "Legal notice" and "Data protection" links
3. - big arrow up: anchor to the top of the page; it moves outside the grid, as soon as there is space, else positioned at the right bottom

## 2. The Centres

this page represents all Centres

### 2.1 Header

1. top white bar holds the title of the page
2. rest -> see 1.1 Home->Header

### 2.2 Main Content of The Centres

1. element: image player
2. element: Our Centres in Africa
    - List of the Centres with a link symbol (this is not an accordion)
3. element: general information about all Centres
    - two sub-elements with two columns, first picture-text second two columns text with the possibility to add pictures
4. element: How it started and who is behind all this
    - 2 columns sub-elements
    - single-Member-display
      - this is a special element that even an editor can use at different places
      - the "single-Member-display" has: professional title, name and organisation the member works for (If a member has many work engagements, it is the first organisation listed on the profile, or choose one from a list.)

### 2.3 Footer

see Footer Home 1.3

## 3. Single Centre Page

### 3.1 Header

1. top white bar holds the Centres name plus the Centre individual logo after the name, fixed align to right
2. other content of the header -> see 1.1 Home->Header (small logo here for logged-in)

### 3.2 Main Content of a "Single Centre Page"

1. element:
    - full name, links to sub-pages, main image for the Centre
    - link icons: please used embedded SVGs to reduce server requests
2. element: video button
    - this opens a video as an overlay over the whole screen
    - videos are embedded by an iframe, directing to a folder on the same server which holds the video content
    - for videos it is HLS and MPEG-DASH plus a fallback stream
    - editors should have an easy way to embed videos (e.g. iframe); Please note: GDPR rules makes YouTube impossible!
3. element: Content and Goals
    - two columns, text editor with image possibility and standard "single-Member-display" -> see 2.2.4
4. element: two columns with a nested two columns
    - standard "single-Member-display
5. element: Latest Blogs from this Centre
    - same as on Blogs, but this time blogs only from this Centre
    - no drop-down menu "search and view all" goes with a link to "All Blogs" with an anchor to the **open** search field see HOME -> 1.2.2
    - **The layout of the blogs preview has changed please see the blogs preview on the layout pdf for "Blogs"! Now social media, category, date and authors name are added!**
6. element: image player
    - has autoplay
    - has caption underneath
    - all image sliders should have slide in arrows on hover
7. element: Recommended Topics
    - same element as on Home but my has different links
    - see Home -> 1.2.3
8. element: Our Members and Alumni strive for Excellence
    - randomly displayed members and alumni, but only from THIS Centre, the reload circle-button loads random members only from THIS Centre
    - see Home -> 1.2.6
    - no drop-down menu "search and view all" goes with an anchor to the all members page search area
    - IMPORTANT: on members page we need an mandatory entry to which centre someone belongs! (My Centre/DAAD/external)
9. element: Recommended Moodle courses
    - the 4 latest Moodle courses that are offered from THIS Centre
    - see Home -> 1.2.8
    - no drop-down menu "search and view all" goes to a landing page with the Moodle courses of a specific centre inside Moodle

### 3.3 Footer

standard footer, see Home -> 1.3

## 4. Partners

the partners of a Centre

### 4.1 Header

1. top white bar holds a short version of the Centres name, plus the word "Partners"
2. other content of the header -> see 1.1 Home->Header

### 4.2 Main Content of the pages for "Partners"

1. element: Headline plus links
    - a headline and links to sub-pages and back to the Centre
    - link icons: please used embedded SVGs to reduce server requests
2. element: this is a standard element that can be used often
    - there are two different kinds of partners:
      1. Project Partners
      2. Network Partners
    - the right column has standard "single-member-display" see -> The Centres 2.2.4

### 4.3 Footer

standard footer, see Home -> 1.3

## 5. Contact

the contact details of a Centre

### 5.1 Header

1. top white bar holds a short version of the Centres name, plus the word "Contact"
2. other content of the header -> see 1.1 Home->Header

### 5.2 Main Content of the pages for "Contacts"

1. element: Headline plus links
    - a headline and links to sub-pages and back to the Centre
    - link icons: please used embedded SVGs to reduce server requests
2. element: OpenStreetMap element with icons on it for the Centre and it's partners
    - the right column has standard "single-member-display" see -> The Centres 2.2.4
    - According to GDPR you must first ask users before you submit data to load the map-tiles! First display a normal image. Underneath we have a load OSM link "activate interactive open street map".
3. element: How to find us on Campus in …
    - two columns
      - left: text plus image below link to download a pdf
      - right: here we have contact details for the Centre
4. element: two columns text and standard "single-member-display" see -> The Centres 2.2.4
    - underneath the "single-Member-display" we need an email address link
5. element: blue background, three columns with DAAD addresses to contact
    - these addresses are different for northern and southern Africa

### 5.3 Footer

standard footer, see Home -> 1.3

## 6. Blog

This is the search and overview page for blogs. Blogs are also used for short news to publish. For a very short notice to the members, there are also the header space and the home slogan space that can be used!

### 6.1 Header

1. top white bar holds a headline "Our Blogs with News and Publications"
2. other content of the header -> see 1.1 Home->Header

### 6.1.1

1. a button "contribute/write a new blog"

### 6.2 Main Content for Blogs

1. element: This is a slider that holds for each category the latest blog.
    - the plus symbol opens a social media bar, to recommend a blog in the social media
    - we have the publishing date, the category and the name of the author
    - the headline and the fist part of the copy text is shown / headline is a link to single blog display of "this"
    - the read more link should have the symbol for links
2. element: Our most successful blogers
    - here we have our standard "single-Member-display" but in lardger
    - underneath each member is a link to the latest blog of the hero
    - the list display of the persons blogs is not related to one special blog but to a person (Best ratings of all blogs of a person, not the best rated single blog and then the author!)
3. element: Chosen from our editor
    - this is a medium preview from a blog chosen by the admin, this blog could also be longer, then it needs a "more link" in the end
    - the standard "single-Member-display" of the author is now horizontal and smaller
4. element: blog search
    - same system like "member search" see -> 9.2.1 Members, but we have different items to narrow down the search to certain areas: a date range of publication, a Centre (choose from drop-down), a university (choose from drop-down), a country (choose from drop-down) or a category (choose from drop-down)
5. element: all blogs and search results
    - the aim was here to show as many blogs as possible on a small space, so it is just the headline (maybe we have to limit it to a certain number of characters and then add dots), date, category and authors name.
    - search results are shown in a page mode with indexes at the bottom
    - for the start, before search results are displayed, it shows random blogs

### 6.3 Footer

standard footer, see Home -> 1.3

## 7. Single Blog

This is the single view of one single blog.

### 7.1 Header

1. top white bar shows the name of the author as a headline: Blog from …
2. other content of the header -> see 1.1 Home->Header

### 7.2 Main Content for a Single Blog

1. element: The blog
    - top: date, category and author
    - headline and then main blog picture
    - all elements in the blog according to the Gutenberg WP system
    - videos should also be possible (i-frame, stream in HLS, MPEG-DASH and fallback)
    - in the bottom users can rate this blog (only for members or for all public must be decided; for members we need a control system, that they are not rating serveral times)
    - the author is displayed in "single-member-display" see -> The Centres 2.2.4
    - category, keywords and previous or next topic, like in all common blogs
    - comments and captcha
2. element: Search blogs
    - here we can search for blogs, with a redirect for results to the page "Blog"
    - the element is the same as on the page "Blog" -> 6.2.4

### 7.3 Footer

standard footer, see Home -> 1.3

## 8. Learn

This is the Moodle start page and from there to all Moodle subpages.
Apply the general DIGIFACE CI published as a PDF to all Moodle pages. Have our main icon navigation on top of all Moodle page that are not inside a course.

Start page grouped with Africa map icons in accordance to the Centres. (see layout from Paul)

## 9. Projects

This is the WP project manager and needs a general start page with all projects listed that want to be public.

1. content
    - project leader
    - headline
    - short description
    - corresponding Centre of the project or "general project" not corresponding to a single Centre

Apply the general DIGIFACE CI published as a PDF to all WP-project pages.

## 10. Members

The page to search for members who are part of the community.

### 10.1 Header

1. top white bar holds a headline "Members and Search"
2. other content of the header -> see 1.1 Home->Header

### 10.2 Main Content for Members

1. element: dedicated to search for members, opens up on a click
    - click on the magnifier or the down arrow
    - details of the search layout please enable the layer in the photoshop design document
    - search:
        - it is a search that can be narrowed down by checking the fields below or choose a restriction from the drop-down e.g.: "search for all Millers in Ghana, which are among staff"; empty search field e.g. "search for everyone who is from DAAD and is part of a project".  
        Similar behavior as Google picture search, where you can sarch only for e.g. small images.
    - new (202001028) is an additional box for narrowing down the search to "external" persons / tick box
2. element: Members
    - this is the display area for the search
    - without search it displays random members
    - member display: picture, Name, first entry on "current professional activity" -> see members/single member
    - you can sort the search results and the random display in an alphabetical order (up and down)
    - search results are presented in a page-manner with page indexes

### 10.3 Footer

standard footer, see Home -> 1.3

## 11. Personal Profile

This is the page that shows an individual single member.

**It has different views for logged-out and logged-in!**

- logged-out it shows content that was enabled for the "world" by the profile owner
- logged-in it shows all content for this user and edit fields
- for other members who are logged-in it shows content that was enabled for all members by this profile owner
- content can be enabled for (radio button):
  1. the world
  2. all members
  3. hidden for everybody except the profile owner himself
  4. (later on we can add a "friends" lable, but "friends" is not implemented yet)

### general information from the project paper

There will be different information levels for building up user profiles. Relevant data have to be collected by asking users and members to provide information and giving their consent to further data treatment.

- The first level of data concerns information that can be filled by the member without control:  
name, professional title, phone number, email, website, address, portrait picture, videos other pictures, CV in general ...
- The second level of information needs to be reviewed and activated by a Admin-Member only:
major academic qualifications (hint for the user: "will be shown only after approval by the adminstrator)
- The third level of information is generated by the system
completed Moodle courses ongoing or started Moodle courses (User should have the possibility to delete courses, if not completed.)
- The fourth level concerns "System information" and is monitored by a Admin-Member:  
user role and permissions, activity figures, if generated by the system.
- mandatory information is: name, my Centre (or DAAD, other), country of living, town of living
- mandatory is status: student, alumni, lecturer, staff, DAAD, other

### 11.1 Header

1. top white bar holds a headline with the name of the member
2. other content of the header -> see 1.1 Home->Header

### 11.2 Main Content for Personal Profile

The top part of the main content is in 2 columns, the right column (except of the first field) is for future use when we will implement the social communication features like: friends, groups, internal mail, chat …

1. element: top right: some statistics about the profile (all elements below are not implemented yet)
2. element: top left:
    - Header background picture and profile picture
    - "send internal message" is not implemented yet, if we could use Moodles internal message system for everyone (also people who are not in any Moodle course) would be perfect!
    - a brief description for the member (restricted to 155 characters)
    - country and city where I'm living now
    - current professional activities: status and institute; can be many
    - a slogan or philosophy of live (restricted to 100 characters)
    - text-area with headline
    - a video-link if there is a video on the system about the person -> see also 3.2.2 single Centre page, overlay video, same system here
3. element: Contact
    - professional and personal contact details (in a blue box); about enabling content to the world, see above -> 10. Personal Profile, beginning
4. element: My Professional Experiences (this is the past of a person)
    - we have 3 different labels:
        1. working for an institute or a company: the symbol of a building
        2. working on your own as a single person: the chair
        3. doing voluntary work without payment for a NGO or a club etc.: the world icon
    - the arrow on the right opens a bigger text-area
    - it always has a start and end date or "until today"
    - Languages with a rating from 1=very basic to 10 mother tongue
    - skills and interests keywords, we also need for the search
    - Qualifications
      - this is proper degrees and must be approved by an admin
      - there should be messages to an admin for "open for approval"
5. element: Publications, this is real publications, like books that stay out there in the world
    - the shopping chart is a link to a book shop, or a page that gives information how I can get hold of the book or publication (authors home page or page of a library)
6. element: Internal Publications and Projects; this is internal stuff from the platform
    - links to elements that have been published by the person on the system in the publications module; see -> 12. Publications. This can be even small things like info-graphics or big things like a complete PhD thesis.  
    Here we need a connection (pull) from WP to the WP-publications extension.
    - Projects from WP-projects that the person is taking part.  
    Here we need a connection (pull) from WP to the WP-projects extension.
7. element: Latest Blogs from this person
    - typo: the headline in the layout is wrong it should be: "Latest Blogs from Prof. Klaus Maier-Vorderhöfer"
    - just blogs that have been published by this person, if none the element should not be show
    - for Latest Blogs see also -> Home 1.2.2
8. element: Recommended Moodle Courses from Prof. …
    - Moodle courses that have been published by this person, if none the element should not be show
    - for recommended Moodle courses see also -> Home 1.2.8
    - if possible Moodle should show courses from this person on the linked Moodle start page (get-link)
9. element: Personal
    - this is the space for personal stuff
    - the birthdate can be hidden see above -> 10. Personal Profile, enabling
    - this element has a slider that can be filled by the member, captions are displayed underneath the slider

### 11.3 Footer

standard footer, see Home -> 1.3

## 12. Alumni Network Tumefika

Her we present in the drop-down of the main header menu the links to the Tumefika top level pages.
[Tumefika homepage](https://www.aen-tumefika.org/)

Tumefika must add our top menu to all of their pages!
Tumefika must integrate our blogs that have been written by alumni.

## 13. Publications

This is a WordPress shop theme, that holds publications from the members in a easy searchable shop form.
Instead of buying we have download options as Latex, pdf, epub … documents.

## 14. login

The general login page on WP.  
This is the only login entry for all modules!  
It has register, login, logout and lost password.

## 15. Legal notice

With GDDPR we have clear regualtions what has be here.
Most important a legal responsible person or organisation and clear contact details.

## 16. Data protection

With GDDPR we have clear regulations what has to be here.
Most important is information what is happening exactly with personal data that could be gained through the platform. If any data go to America, or any other country, a clear opt-out BEFORE data are send and even after someone has agreed before. Everyone who has excess to the data on the server needs a data protection contract with the responsible owner of the platform.

## END
