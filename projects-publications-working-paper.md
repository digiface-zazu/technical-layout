# DIGIFACE working paper for DOCUMENTS and PROJECTS

V0.1.1 20200518

## PUBLICATIONS

### general purpose publications

- present documents from inside the platform to the world or to the members
- this is something like a library or a publisher, it is not a private document repository

### specification publications

- any kind of document (txt, doc, pdf, EPUB, jpg, tif, zip, gzip, tar …)
- each document has a member of the platform who it belongs to (owner/publisher)
- each document has copy right settings, or the kind of usage that is allowed --> suggestions, list or free entry
- each document has a creation date
- each document has a published on the platform date
- each document can have an updated date, so you can change the document but the meta data stay
- each document has a size number
- each document has a preview jpg or the system creates one
- each document has an ID
- each document has a short description (length min/max ?)
- each document has a title
- each document has at least one keyword
- each document has a category (choose from a list, what is on the list?)
- each document can get user ratings
- each document can get an academic rating (like a master thesis or a PhD work, what are the numbers?)
- each document can be downloaded
- each document has a permalink, so someone can always refer to it in other publications
- each document has a number how often it was downloaded
- each document has a visibility: no, for members, to the world

### demands

- the documents should be easy searchable
- ratings (average) and downloaded number should be displayed
- do we need the possibility to comment?

## PROJECT MANAGEMENT

### general purpose project management

- working together with one or more members of the platform on a project
- this could be a simple assignment of some students or a big research project of many academics around the world with a huge amount of documents and tasks

### specification project management

- managing of tasks (the Redmine software could be a base)
- storing and grouping of documents
- write documents together with a version and author control (could be easy with Latex and git, possible with Libre Office?)
- calendar functions with milestones
- Kanban boards
- waterfall diagrams
- Srcum boards
- messaging
- leaving notes
- online video meetings
- chat

### demands project management

- the base is documents that people work on
- comments and versions are important
- a data base of documents is always there
- communication is essential

## Ideas from the DIGIFACE kick-off meeting work group

### IT/Project Management Tool

#### Which functionalities of the Project Management Tool of DIGI-FACE are needed?

- should simulate requirements when projects are being assessed (time frame, needed requirements, calendar, timeline, reminder for critical steps)
- workflow of different teams
- project networking if 2 or 3 Centres work on a shared project
- survey organising data related
- documentation to show consent and adhering to ethics
- platform to see what others are doing i.e. collaborative writing support
- completion tracking and assigning of responsibility
- product breakdown structure and work breakdown structure
- possibility to integrate tools already used i.e. Trello
- dashboards to provide broader overview of project
- possibility to integrate analysis software
- legal issues (GDPR)
- team profiles for staff
- tool should be useful for all Centres not just DIGI-FACE
- integration with redcap that is used for medical research (Ghana/Accra)
- Should we not just integrate a tool like MS Teams? (Not all Centres use Microsoft but MS Team is free
for educational use)
- Platform independence is important
- support for all stages of project management from initial proposal and budgets
- finances:
- reporting to supervisors + funding bodies
- summary reports for alumni support for multiple levels of breakdown i.e. including tutors in a larger
project
- There is a difference between an engineering project, training project, medical research project etc.
- custom workflow examples for different project types would be excellent
- support for brainstorming sessions