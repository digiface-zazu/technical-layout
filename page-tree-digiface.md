# Page layout DIGIFACE

updated 20210528

## Page Tree

- 1 Home
  - About DIGIFACE
  - FAQ
- 2 Programmes general page
  - About African Excellence
    - single Centre page
      - Partners
      - Contact
      - additional page if required
    - single Centre page
      - Partners
      - Contact
      - additional page if required
  - About any other Programme that takes part
    - single other programme page
      - Partners other programme
      - Contact other programme
      - additional page if required for other programme
    - single other programme page
      - Partners other programme
      - Contact other programme
      - additional page if required for other programme
- 3 Blog list of Blogs
  - Single blog article
- 4 Learn WP page about Learning
  - Moodle starting page
  - Moodle subpages
  - …
- 5 Publications start page with search
  - categories list page (each category)
  - search result page
    - single publication page
- 6 Projects start page overview
  - single project page
    - pages inside a project (issue board like Asana)
    - …
- 7 Members list start page
  - members search result pages
    - single member profile page
    - each member has a page …
- 8 Alumni-Network (alumni external page)
  - alumni subpages (external)
- 9 general search results page
- 10 Login/logout
- 11 Legal notice
- 12 Data protection
- XX Backend pages
  - my profile backend
  - … more backend pages …