# Technical Description of the Moodle area of the DIGIFACE app

V 0.0.1 / zazu.berlin Thomas Hezel / Merlin Kull Kehl 20201029
View the latest version of this document on [Bitbucket in the release branch](https://bitbucket.org/digiface-zazu/technical-layout/src/release/)
Document name: technical-layout-app-moodle-area.md

## 1. "Start Page" APP MOODLE AREA

entry point to the learning area

### 1.1. Header

1. top navigation line (same as app general)
    - language selection
    - login / logout
    - Dashboard (BE of WordPress)
    - My Profile
    - Menu bars
2. top communication line (same as app general)
    - contacts
    - messages (3 bubble icons in different colors for messages: from any user, from friends, official)
    - button to messages
    - button general settings for the app
3. Headline: Learn
4. bit smaller navigation line with:
    - all courses
    - my courses
    - magnifying glass icon for search
5. omit the settings navigation with the three dots icon, that you will find in the orgiginal moodle app and put the settings into moodle settings

### 1.2. Body of the Start Page

1.Body
    - Headline: "courses from our Centres of Excellence"
    - use the map icons as they are now
    - make the map icons a link
    - make the map icons plus description smaller so on portrait mode we have two centres in one row
    - omit the maginfying glas for the image

### 1.3. Footer of the Start page

In comparison to the original moodle app on the start page we don't have a footer navigation

## 2. "All Courses" APP MOODLE AREA

### 2.1. Header

Header same as 1.1. Start Page

### 2.2. Body of the page "All Courses"

List in small letters with all courses available: course name; category/centre
    a. alphabetical sorting
    b. latest/newest

### 2.3. Footer

no footer on this page

## 3. "My Courses" APP MOODLE AREA

### 3.1. Header

Headline: "My Courses"

### 3.2. Body

display Moodle theme as in the original Moodle app

### 3.3. Footer

This footer applies for all pages that are displayed for a logged-in user inside the learning area.
The footer has two areas marked with different background colors
    a. left: communiction and Home
    b. right: Moodle items

a. Left area
    - Home icon with target: "Start Page" APP MOODLE AREA (from there users can get back to other WP pages)
    - contacts icon
    - three message bubble icons
    - messages button
b. Right area
    - Moodle calender
    - Moodle system notifications
    - Moodle settings icon

The icons can be in two or three rows and take enough space on the footer. But then:
Both areas can be slided down to a simple color line with an up-pointing arrow, if there are notifications or messages coming in the color lines should blink. Users can then slide up the area to a complete footer

## 4. "Search Courses" APP MOODLE AREA

### 4.1. Header

Some Header as Moodle-App Start Page 1.1.

### 4.2. Body

see original Moodle app

### 4.3. Footer

no footer

## 5. "Single Course" APP MOODLE AREA

### 5.1. Header

Show course title in full length! Display it emphasised so users realize that they are now inside a course!
Submenu behind the three dots as is on Moodle original app.

### 5.2. Body

As Moodle original app

### 5.3. Footer

As 3.3. general inside Moodle learning area footer.

## 6. "Courses of a Centre" First Page

### 6.1. Header

1. top navigation line (same as app general)
    - language selection
    - login / logout
    - Dashboard (BE of WordPress)
    - My Profile
    - Menu bars
2. top communication line (same as app general)
    - contacts
    - messages (3 bubble icons in different colors for messages: from any user, from friends, official)
    - button to messages
    - button general settings for the app
3. Headline: "abbreviation of the centres name" plus "courses" e.g. CCAM-Courses

### 6.2. Body

As is in original Moodle app, but show full name of the centre in the folded top area.

### 6.3. Footer

no footer

## 7. "Courses of a Centre" Second Page

### 7.1. Header

1. top navigation line (same as app general)
    - language selection
    - login / logout
    - Dashboard (BE of WordPress)
    - My Profile
    - Menu bars
2. top communication line (same as app general)
    - contacts
    - messages (3 bubble icons in different colors for messages: from any user, from friends, official)
    - button to messages
    - button general settings for the app
3. Headline: "abbreviation of the centres name" plus "courses" e.g. CCAM-Courses

### 7.2. Body

As is in original Moodle app, but show full name of the centre in the folded top area.

### 7.3. Footer

no footer

## END
